Feature: Log in to the system
        As a a registered user
        I can log in to the system so that 
        I can start using the system.
Scenario: log in with correctly registered credentials
    Given I am a registered user with <email> 
    When I log in to the system with correct <email> and <password>
    Then I should be loged in to the system
    Examples:
        |   email|  password|
        |   jhon@gmail.com|  secret|
        |   jane@gmail.com|  secret|

Scenario: log in with Unregistered credentials
    Given I am not a registered with <email> 
    When I log in to the system with <email> and <password>
    Then I should get error indicating that I am not registered
    Examples:
        |   email|  password|
        |   user1@gmail.com|  secret|
        |   user2@gmail.com|  secret|


Scenario: log in with wrong password
    Given I am a registered user with <email> 
    When I log in to the system with correct <email> and icorrect <password>
    Then I should get error indicating that My password was incorrect
    Examples:
        |   email|  password|
        |   jhon@gmail.com|  notsecret|
        |   jane@gmail.com|  notsecret|
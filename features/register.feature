Feature: user registration to the system
        In order to log in to the system
        As a a user 
        I can register to the system so that 
        I can log in to the system and user services.
    
Scenario: registeration with correct email, firstname and last name
    Given I have provided <email>,<firstname>,<lastname> and <password>
    When I submit the registration form
    Then I should be registered to the system
    Examples:
        |   email|  firstname|  lastname|   password|
        |   jhon@gmail.com|  jhon|  doe|   secret|
        |   jane@gmail.com|  jane|  doe|   secret|


Scenario: registration with invalid input
    Given I have provided incorrectly formated <email>,<firstname>,<lastname> and <password>
    When I submit the registration form
    Then I should get an error saying invalid input
    Examples:
        |   email|  firstname|  lastname|   password|
        |   jhongmail.com|  jhon|  doe|   secret|
        |   jane@gmail|  jane|  doe|   secret|


Scenario: registration with not completed
    Given I have not provided one of <email>,<firstname>,<lastname> and <password> inputs
    When I submit the registration form
    Then I should get an error saying incomplete input
    Examples:
        |   email|  firstname|  lastname|   password|
        |   jhon@gmail.com|  jhon|  doe|   |
        |   jane@gmail.com|  jane|  |   secret|
        |   mock@gmail.com|  |  doe|   secret|
        |   |  jane|  doe|   secret|


Scenario: registration of already registered user
    Given I have already registered with<email>,<firstname>,<lastname> and <password> 
    When I submit the registration form
    Then I should get an error saying already registered user
    Examples:
        |   email|  firstname|  lastname|   password|
        |   jhon@gmail.com|  jhon|  doe|   secret|
        |   jane@gmail.com|  jane|  doe|   secret|